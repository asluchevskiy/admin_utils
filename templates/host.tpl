{% if make_www %}server {
    listen 80;
    server_name www.{{ host }};
    return 301 $scheme://{{ host }}$request_uri;
}{% endif %}

server {
    listen 80;
    server_name  {{ host }};
    root   {{ root }};
    index  index.php;
    access_log {{ access_log }};
    error_log {{ error_log }};

    location / {
        try_files $uri $uri/ /index.php?q=$uri&$args;
    }
    location ~* ^.+.(js|css|png|jpg|jpeg|gif|ico)$ {
        access_log        off;
        expires           max;
    }
    location ~ \.php$ {
        # fastcgi_split_path_info ^(.+\.php)(.*)$;
        fastcgi_pass   {{ fastcgi_path }};
        fastcgi_index  index.php;

        include fastcgi_params;
        fastcgi_param  QUERY_STRING     $query_string;
        fastcgi_param  REQUEST_METHOD   $request_method;
        fastcgi_param  CONTENT_TYPE     $content_type;
        fastcgi_param  CONTENT_LENGTH   $content_length;
        fastcgi_intercept_errors        on;
        fastcgi_ignore_client_abort     off;
        fastcgi_connect_timeout 60;
        fastcgi_send_timeout 180;
        fastcgi_read_timeout 180;
        fastcgi_buffer_size 128k;
        fastcgi_buffers 4 256k;
        fastcgi_busy_buffers_size 256k;
        fastcgi_temp_file_write_size 256k;
    }
    
    location = /favicon.ico {
        log_not_found off;
        access_log off;
    }
    location = /robots.txt {
        allow all;
        log_not_found off;
        access_log off;
    }
    ## Disable viewing .htaccess & .htpassword 
    location ~ /\.ht {
        deny  all;
    }
}
