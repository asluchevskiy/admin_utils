# -*- coding: utf-8 -*-
import os
import sys
import optparse
import jinja2
import settings
import trans
import glob

def recursive_chown(path, uid=-1, gid=-1):
    for item in glob.glob(path+'/*'):
        if os.path.isdir(item):
            recursive_file_permissions(os.path.join(path,item),mode,uid,gid)
        else:
            try:
                os.chown(os.path.join(path,item),uid,gid)
            except:
                print('File permissions on {0} not updated due to error.'.format(os.path.join(path,item)))



def create_host(host=None, slug=None, make_www=True):
    if not host:
        print 'You need to define host'
        return
    if not slug:
        slug = unicode(host).encode('trans/slug')

    config_path = os.path.join(settings.NGINX_DIR, 'sites-available', slug)
    if os.path.exists(config_path):
        print 'Virtual host %s already exists' % host
        return

    access_log = os.path.join(settings.LOG_DIR, '%s_access.log' % slug)
    error_log = os.path.join(settings.LOG_DIR, '%s_error.log' % slug)
    root = os.path.join(settings.ROOT_DIR, slug+'/')

    tpl_body = open('templates/host.tpl').read()
    t = jinja2.Template(tpl_body)
    with open(config_path, 'w') as fw:
        fw.write(t.render(host=host, slug=slug, make_www=make_www,
                          access_log=access_log, error_log=error_log, root=root,
                          fastcgi_path=settings.FASTCGI_PATH))

    os.symlink(config_path,
               os.path.join(settings.NGINX_DIR, 'sites-enabled', slug))
    os.makedirs(root)
    recursive_chown(root, 33, 33)

    with open(access_log, 'w') as fw:
        pass

    with open(error_log, 'w') as fw:
        pass

    os.chown(access_log, 33, 33)
    os.chown(error_log, 33, 33)


if __name__ == '__main__':
    parser = optparse.OptionParser("usage: %prog [options]")
    parser.add_option('-d', '--host', action='store', dest='host', default=None, help='host (domain) name')
    parser.add_option('-s', '--slug', action='store', dest='slug', default=None, help='slug name')
    parser.add_option('-w', '--www', action='store_true', dest='www', default=False, help='make www')
    (options, args) = parser.parse_args()

    create_host(host=options.host, slug=options.slug, make_www=options.www)
