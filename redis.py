# -*- coding: utf-8 -*-
import os
import sys
import optparse
import jinja2
import settings
import trans

START_PORT = 6379

def create_configs(number, password):
    t1 = jinja2.Template(open(os.path.join(os.path.dirname(__file__), 'templates/redis.conf.tpl')).read())
    t2 = jinja2.Template(open(os.path.join(os.path.dirname(__file__), 'templates/redis-server.conf.tpl')).read())
    ports = list()
    for i in range(number):
        port = START_PORT + i
        ports.append(port)
        server_num = '' if not i else str(i)
        path1 = os.path.join(os.path.dirname(__file__), 'redis', 'redis%s.conf' %  server_num)
        open(path1, 'w').write(t1.render(num=server_num, port=port, password=password))
        path2 = os.path.join(os.path.dirname(__file__), 'init', 'redis-server%s.conf' %  server_num)
        open(path2, 'w').write(t2.render(num=server_num))
    print 'NUMBER OF SERVERS:', number
    print 'PORTS:'
    print ports


if __name__ == '__main__':
    parser = optparse.OptionParser("usage: %prog [options]")
    parser.add_option('-n', '--number', action='store', type='int', dest='number', default=1, help='number of redis servers')
    parser.add_option('-p', '--password', action='store', dest='password', default=None, help='password for redis')
    (options, args) = parser.parse_args()

    create_configs(options.number, options.password)
