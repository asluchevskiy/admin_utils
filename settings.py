# -*- coding: utf-8 -*-
"""
Developed by Arseniy Sluchevskiy <arseniy.sluchevskiy@gmail.com>
"""
ROOT_DIR = '/home/jeanna/public_html/'
LOG_DIR = '/home/jeanna/log/'
FASTCGI_PATH = 'unix:/var/run/php5-fpm.sock'
NGINX_DIR = '/etc/nginx'
